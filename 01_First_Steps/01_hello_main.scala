object HelloMain
{
    def main(args: Array[String]) 
    {
    if (args.size == 0)
        println("Hello World!") 
    else
        println("Hello, " + args(0))
    }
}