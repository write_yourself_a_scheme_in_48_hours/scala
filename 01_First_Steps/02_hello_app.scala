object HelloApp extends App {
    if (args.size == 0)
        println("Hello World!")
    else
        println("Hello, " + args(0))
}
